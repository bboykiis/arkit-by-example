//
//  DollarBill.swift
//  arkit-by-example
//
//  Created by Kristo Kiis on 17/11/2017.
//  Copyright © 2017 CB. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

class DollarBill: SCNReferenceNode {
    var modelName: String {
        return referenceURL.lastPathComponent.replacingOccurrences(of: ".scn", with: "")
    }
    
}
