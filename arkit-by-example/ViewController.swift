//
//  ViewController.swift
//  arkit-by-example
//
//  Created by Can Bal on 8/5/17.
//  Copyright © 2017 CB. All rights reserved.
//

import ARKit
import SceneKit
import UIKit
import ARVideoKit

class ViewController: UIViewController, UIPopoverPresentationControllerDelegate, ARSCNViewDelegate, UIGestureRecognizerDelegate, SCNPhysicsContactDelegate {

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet var messageViewer: MessageView!
    
    // A dictionary of all the current planes being rendered in the scene
    var planes = [UUID: Plane]()
    var planesHidden: Bool = false
    // A list of all the cubes being rendered in the scene
    var cubes: [SCNNode] = []
    
    // Config parameters
    var config: Config = Config()
    var arConfig: ARWorldTrackingConfiguration = ARWorldTrackingConfiguration()
    
    // Recording
    var recordButton : RecordButton!
    var recorder: RecordAR?
    var progressTimer : Timer!
    var progress : CGFloat! = 0
    
   override func viewDidLoad() {
        super.viewDidLoad()
    
        setupScene()
        setupCamera()
        setupLights()
        setupPhysics()
        setupRecognizers()
        setupRecording()
        
        // Create an ARSession config object we can re-use
        arConfig.isLightEstimationEnabled = true
        arConfig.planeDetection = .horizontal
    
        sceneView.preferredFramesPerSecond = 120
    
        config.showStatistics = true
        config.showWorldOrigin = false
        config.showFeaturePoints = true
        config.showPhysicsBodies = false
        config.detectPlanes = true
        updateConfig()
        recorder = RecordAR(ARSceneKit: sceneView)
        print("sceneView frame rate:" + String(sceneView.preferredFramesPerSecond))

        // Stop the screen from dimming while we are using the app
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Run the view's session
        sceneView.session.run(arConfig, options: ARSession.RunOptions(rawValue: 0))
        showMessage(message: "Tracking is initializing")
        
        let configuration = ARWorldTrackingConfiguration()
        recorder?.prepare(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
        recorder?.rest()
    }
    
    func updateConfig() {
        let opts: SCNDebugOptions = [ARSCNDebugOptions.showFeaturePoints]
        sceneView.debugOptions = opts
        sceneView.showsStatistics = true
    }
    
    func setupScene() {
        sceneView.delegate = self
        sceneView.antialiasingMode = .multisampling4X
        let scene = SCNScene()
        sceneView.scene = scene
    }
    
    func setupLights() {
        sceneView.automaticallyUpdatesLighting = true
        sceneView.autoenablesDefaultLighting = false
        if let environmentMap = UIImage(named: "Models.scnassets/sharedImages/environment_blur.exr") {
            sceneView.scene.lightingEnvironment.contents = environmentMap
        }
    }
    
    func setupCamera() {
        guard let camera = sceneView.pointOfView?.camera else {
            fatalError("Expected a valid `pointOfView` from the scene.")
        }
        /*
        camera.wantsHDR = true
        camera.exposureOffset = -1
        camera.minimumExposure = -1
        camera.maximumExposure = 3
         */
    }
    
    func setupRecording() {
        recordButton = RecordButton(frame: CGRect(x: 0,y: 0,width: 70,height: 70))
        recordButton.center = self.view.center
        recordButton.progressColor = .red
        recordButton.closeWhenFinished = false
        recordButton.addTarget(self, action: #selector(ViewController.record), for: .touchDown)
        recordButton.addTarget(self, action: #selector(ViewController.stop), for: UIControlEvents.touchUpInside)
        recordButton.center.x = self.view.center.x
        view.addSubview(recordButton)
    }
    
    func setupPhysics() {
        let bottomPlane = SCNBox(width: 1000, height: 0.5, length: 1000, chamferRadius: 0)
        let bottomMaterial = SCNMaterial()
        bottomMaterial.diffuse.contents = UIColor(white: 1.0, alpha: 0.0)
        bottomPlane.materials = [bottomMaterial]
        let bottomNode = SCNNode(geometry: bottomPlane)
        bottomNode.position = SCNVector3Make(0, -10, 0);
        bottomNode.physicsBody = SCNPhysicsBody(type: .kinematic, shape: nil)
        bottomNode.physicsBody?.categoryBitMask = CollisionCategory.bottom.rawValue
        bottomNode.physicsBody?.contactTestBitMask = CollisionCategory.cube.rawValue
        sceneView.scene.rootNode.addChildNode(bottomNode)
        sceneView.scene.physicsWorld.contactDelegate = self
    }
    
    func setupRecognizers() {
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(insertCubeFrom(_:)))
        sceneView.addGestureRecognizer(panGestureRecognizer)
        
        // Press and hold with two fingers causes explosion
        let explodeGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(explodeFrom(_:)))
        explodeGestureRecognizer.minimumPressDuration = 1
        explodeGestureRecognizer.numberOfTouchesRequired = 2
        sceneView.addGestureRecognizer(explodeGestureRecognizer)
    }
    
    @objc func insertCubeFrom(_ sender: UIPanGestureRecognizer) {
        let velocity = sender.velocity(in: sceneView)
        if (sender.state == .ended) {
            if velocity.y < 0 {
                insertCube(velocity: velocity.y)
            }
        }
        
    }
    
    func getUserVector() -> (SCNVector3, SCNVector3) { // (direction, position)
        if let frame = self.sceneView.session.currentFrame {
            let mat = SCNMatrix4(frame.camera.transform) // 4x4 transform matrix describing camera in world space
            let dir = SCNVector3(-1 * mat.m31, -1 * mat.m32, -1 * mat.m33) // orientation of camera in world space
            let pos = SCNVector3(mat.m41, mat.m42, mat.m43) // location of camera in world space
            return (dir, pos)
        }
        return (SCNVector3(0, 0, -1), SCNVector3(0, 0, -0.2))
    }
    
    func insertCube(velocity: CGFloat) {
        //self.playSoundEffect(ofType: .torpedo)
        print("Velocity:" + String(describing: velocity))
        let (direction, position) = self.getUserVector()
        var newPosition = SCNVector3(x:position.x, y:position.y-0.01, z:position.z)
        newPosition.y = position.y-0.02
        let assetsURL = Bundle.main.url(forResource: "Assets.scnassets", withExtension: nil)!
        let modelsURL = assetsURL.appendingPathComponent("Models")
        let dollarURL = modelsURL.appendingPathComponent("dollar")
        let dollarStringUrl = dollarURL.absoluteString + "dollar.scn"
        let finalURL = URL.init(string: dollarStringUrl)
        
        let cube = DollarBill.init(url: finalURL!)
        cube?.load()

        cube?.position = newPosition
        cube?.childNodes.first?.look(at: direction)
        cubes.append(cube!)
        
        let _force = SCNVector3(x: direction.x*0.002, y: 0 , z: direction.z*0.002)
        let forcePosition = SCNVector3(x: 0, y: 0, z: 0)
        cube?.childNodes.first?.physicsBody?.applyForce(_force, at: forcePosition, asImpulse: true)
        sceneView.scene.rootNode.addChildNode((cube?.childNodes.first)!)
        
    }
    
    @IBAction func detectPlanesChanged(sender: UISwitch) {
        let enabled = sender.isOn
        config.detectPlanes = enabled
        if (enabled) {
            disableTracking(disabled: false)
        } else {
            disableTracking(disabled: true)
        }
    }
    
    func disableTracking(disabled: Bool) {
        // Stop detecting new planes or updating existing ones.
        var opts: SCNDebugOptions = []
        if (disabled) {
            arConfig.planeDetection = []
            for plane in planes {
                plane.value.hide()
            }
        } else {
            opts.insert(ARSCNDebugOptions.showFeaturePoints)
            arConfig.planeDetection = .horizontal
            for plane in planes {
                plane.value.reveal()
            }
        }
        sceneView.debugOptions = opts
        sceneView.session.run(arConfig)
    }
    
    func refresh() {
        planes.removeAll()
        cubes.removeAll()
        sceneView.session.run(arConfig, options: [.resetTracking, .removeExistingAnchors])
    }
    
    // MARK: - SCNPhysicsContactDelegate
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        let nodeACategoryBitMask = contact.nodeA.physicsBody?.categoryBitMask ?? 0
        let nodeBCategoryBitMask = contact.nodeB.physicsBody?.categoryBitMask ?? 0
        let contactMask = nodeACategoryBitMask | nodeBCategoryBitMask
        
        if (contactMask == (CollisionCategory.bottom.rawValue | CollisionCategory.cube.rawValue)) {
            if (nodeACategoryBitMask == CollisionCategory.bottom.rawValue) {
                contact.nodeB.removeFromParentNode()
            } else {
                contact.nodeA.removeFromParentNode()
            }
        }
    }
    
    
    //EXPLOSION
    @objc func explodeFrom(_ sender: UILongPressGestureRecognizer) {
        if (sender.state != .began) {
            return;
        }
        let holdPoint = sender.location(in: sceneView)
        let result = sceneView.hitTest(holdPoint, types: ARHitTestResult.ResultType.existingPlaneUsingExtent)
        if (result.count == 0) {
            return;
        }
        
        let hitResult = result.first!
        DispatchQueue.global(qos: .userInitiated).async {
            self.explode(hitResult)
        }
    }
    
    func explode(_ hitResult: ARHitTestResult) {
        let explosionYOffset: Float = 0.1
        let position = SCNVector3Make(hitResult.worldTransform.columns.3.x,
                                      hitResult.worldTransform.columns.3.y - explosionYOffset,
                                      hitResult.worldTransform.columns.3.z)
        for cubeNode in cubes {
            var distance = SCNVector3Make(cubeNode.worldPosition.x - position.x,
                                          cubeNode.worldPosition.y - position.y,
                                          cubeNode.worldPosition.z - position.z)
            
            let len = sqrtf(distance.x * distance.x + distance.y * distance.y + distance.z * distance.z)
            let maxDistance: Float = 2;
            var scale = max(0, (maxDistance - len))
            
            scale = scale / 1000;
            
            distance.x = distance.x / len * scale;
            distance.y = distance.y / len * scale;
            distance.z = distance.z / len * scale;
            
            cubeNode.childNodes.first?.physicsBody?.applyForce(distance, at: SCNVector3Make(0.05, 0.05, 0.05), asImpulse: true)
        }
    }
    
    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        let estimate = sceneView.session.currentFrame?.lightEstimate
        if (estimate == nil) {
            return
        }
        
        // A value of 1000 is considered neutral, lighting environment intensity normalizes
        // 1.0 to neutral so we need to scale the ambientIntensity value
        let intensity = estimate!.ambientIntensity / 1000.0
        sceneView.scene.lightingEnvironment.intensity = intensity
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if (!(anchor is ARPlaneAnchor)) {
            return;
        }
        let plane = planes[anchor.identifier]
        plane?.update(anchor: anchor as! ARPlaneAnchor)
    }

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if (!(anchor is ARPlaneAnchor)) {
            return;
        }
        let plane = Plane(anchor: anchor as! ARPlaneAnchor, hidden: false, material: Plane.currentMaterial())
        planes[anchor.identifier] = plane
        node.addChildNode(plane)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        planes.removeValue(forKey: anchor.identifier)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, willUpdate node: SCNNode, for anchor: ARAnchor) {
        // noop
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        switch camera.trackingState {
        case .notAvailable:
            showMessage(message: "Camera tracking is not available on this device")
            break
        case .limited(ARCamera.TrackingState.Reason.excessiveMotion):
            showMessage(message: "Limited tracking: slow down the movement of the device")
            break
        case .limited(ARCamera.TrackingState.Reason.insufficientFeatures):
            showMessage(message: "Limited tracking: too few feature points, view ares with more textures")
            break
        case .normal:
            showMessage(message: "Tracking is back to normal")
            break
        default:
            showMessage(message: "Limited tracking: unknown reason")
            break
        }
    }

    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        showMessage(message: "A session error has occurred")
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        let alert = UIAlertController(title: "Interruption", message: "The tracking session has been interrupted. The session will reset once the interruption has completed", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default)
        
        alert.addAction(ok)

        present(alert, animated: true, completion: {() in })
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        showMessage(message: "Tracking session has been reset due to interruption")
        refresh()
    }
    
    func showMessage(message: String) {
        messageViewer.queueMessage(message: message)
    }
    
    
    @objc func record() {
        self.progressTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(ViewController.updateProgress), userInfo: nil, repeats: true)
        recorder?.record()
    }
    
    @objc func updateProgress() {
        
        let maxDuration = CGFloat(5) // Max duration of the recordButton
        
        progress = progress + (CGFloat(0.05) / maxDuration)
        recordButton.setProgress(newProgress: progress)
        
        if progress >= 1 {
            progressTimer.invalidate()
        }
        
    }
    
    @objc func stop() {
        self.progressTimer.invalidate()
        recordButton.setProgress(newProgress: 0)
        recorder?.stopAndExport()
    }
    
    
}
